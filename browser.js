class Filter {
    constructor(element) {
        this.base = element
    }
    get detailElement() {
        return this.base.querySelector('div.detailMode')
    }
    get editElement() {
        return this.base.querySelector('div.editMode')
    }
    get mode() {
        if (this.detailElement.offsetParent)
            return 'detail'
        else if (this.editElement)
            if (this.editElement.offsetParent)
                return 'edit'
            else {
                console.log('problems with mode')
                return null
            }
    }
    set mode(value) {
        let currentMode = this.mode
        if (value !== currentMode){
            console.log(`switching modes from ${currentMode} to ${value}`)
        }
    }
    get field() {
        let field = {}
        field.value = this.detailElement.querySelector('div.detailContainer').childNodes[0]
            .textContent
            .replace(/['"]+/g, '')
            .trim()
        field.dropdown = {}
        field.dropdown.buttons = {}
        field.dropdown.buttons.dropdown = null
        field.dropdown.element = document.querySelector('div.x-layer.x-combo-list.pc-list')
        field.dropdown.options = null
        field.dropdown.usable = false
        if (this.mode === 'edit') {
            field.dropdown.buttons.dropdown = this.editElement.querySelector('form div.x-box-inner div.x-form-field-wrap:nth-child(2) img')
            if (!field.dropdown.element) {
                field.dropdown.buttons.dropdown.click()
            }
            if (field.dropdown.element) {
                field.dropdown.options = [...field.dropdown.element.querySelectorAll('div.x-combo-list-item')]
            }
        }
        return field
    }
    set field(value) {
        if (this.mode !== 'edit')
            this.buttons.edit.click()
        if (!this.field.dropdown.usable) {
            this.field.dropdown.buttons.dropdown.click()
        }
        this.field.dropdown.options.find(o => o.textContent === value).click()
    }
    get operator() {
        let operator = {}
        operator.value = this.detailElement.querySelector('div.detailContainer').childNodes[1]
            .textContent
            .replace(/['"]+/g, '')
            .trim()
        operator.dropdown = {}
        operator.dropdown.buttons = {}
        operator.dropdown.buttons.dropdown = null
        operator.dropdown.element = document.querySelector('div.x-layer.x-combo-list.pn-list')
        operator.dropdown.options = null
        operator.dropdown.usable = false
        if (this.mode === 'edit') {
            operator.dropdown.buttons.dropdown = this.editElement.querySelector('form div.x-box-inner div.x-form-field-wrap:nth-child(3) img')
            if (!operator.dropdown.element) {
                operator.dropdown.buttons.dropdown.click()
            }
            if (operator.dropdown.element) {
                operator.dropdown.options = [...operator.dropdown.element.querySelectorAll('div.x-combo-list-item')]
            }
        }
        return operator
    }
    set operator(value) {
        console.log(`setting operator to ${value}`)
        if (this.mode !== 'edit')
            this.buttons.edit.click()
        if (!this.operator.dropdown.usable)
            this.operator.dropdown.buttons.dropdown.click()
        this.operator.dropdown.options.find(o => o.textContent === value).click()
    }
    get parameter() {
        let parameter = {}
        parameter.value = this.detailElement.querySelector('div.detailContainer').childNodes[2]
            .textContent
            .replace(/['"]+/g, '')
            .trim()
        parameter.element = null
        if (this.mode === 'edit')
            parameter.element = this.editElement.querySelector('input.pv')
        return parameter
    }
    set parameter(value) {
        console.log(`setting parameter to ${value}`)
        if (this.mode !== 'edit')
            this.buttons.edit.click()
        this.parameter.element.value = value
    }
    get buttons() {
        let buttons = {}
        buttons.edit = this.base.querySelector('span.fLinks a.fLink.editLink')
        buttons.remove = this.base.querySelector('span.fLinks a.fLink.removeLink')
        buttons.okay = this.editElement ? this.editElement.querySelector('table.x-btn.ok button') : null
        buttons.cancel = this.editElement ? this.editElement.querySelector('table.x-btn.cancel button') : null
        return buttons
    }
    get properties() {
        return {
            field: this.field.value,
            operator: this.operator.value,
            parameter: this.parameter.value
        }
    }
    checkUpdated(newProperties) {
        return new Promise((resolve) => {
            const checkUpdated = () => {
                let isUpdated = JSON.stringify(newProperties) === JSON.stringify(this.properties)
                // let currentProperties = this.properties;
                // let sameField = field === currentProperties.field;
                // let sameOperator = operator === currentProperties.operator;
                // let sameParameter = parameter === currentProperties.parameter;
                // let backInDetailMode = this.mode === 'detail'
                // let isUpdated = sameField && sameOperator && sameParameter && backInDetailMode;
                if (isUpdated) {
                    resolve();
                } else {
                    setTimeout(checkUpdated, 100);
                }
            };
            checkUpdated();
        });
    }
    static async create(properties){
        console.log(`properties=`,properties)
        let addFilter = await Filter.addFilter()
        let newElement = await addFilter
        let newFilter = new Filter(newElement)
        await newFilter.update(properties)
        return newFilter
    }
    static async addFilter(){
        const getFilters = () => [...document.querySelectorAll('div.cfPanel_fieldFilterSection div.filterItemContainer')]
        let startFilters = getFilters()
        // click add button if possible
        let addButton = document.querySelector('table#filterPanel_addFilterButton button')
        addButton.click()
        return new Promise((resolve) => {
            const checkUpdated = () => {
                let newFilter = getFilters().find(f => !startFilters.includes(f))
                if (newFilter){
                    resolve(newFilter)
                } else {
                    setTimeout(checkUpdated, 200);
                }
            };
            checkUpdated();
        });
    }
    match({field, operator, parameter} = {}) {
        let currentProperties = this.properties
        return (currentProperties.field === field) &&
            (currentProperties.operator === operator) &&
            (currentProperties.parameter === parameter)
    }
    update({field, operator, parameter} = {}) {
        console.log(field, operator, parameter)
        this.field = field;
        this.operator = operator;
        this.parameter = parameter;
        this.buttons.okay.click();
        return this.checkUpdated({field,operator,parameter})
    }
    async remove() {
        const getFilters = () => [...document.querySelectorAll('div.cfPanel_fieldFilterSection div.filterItemContainer')]
        let startFilters = getFilters()
        this.buttons.remove.click()
        return new Promise((resolve) => {
            const checkRemoved = () => {
                let currentFilters = getFilters()
                let removedFilter = startFilters.find(f => !currentFilters.includes(f))
                if (removedFilter){
                    resolve(removedFilter)
                } else {
                    setTimeout(checkRemoved, 200);
                }
            };
            checkRemoved();
        });
    }
}

class ReportFilter {
    constructor(element) {
        this.base = element
    }
    get field() {
        return {value: this.base.querySelector('span.filterField').textContent}
    }
    get operator() {
        return {value: this.base.querySelector('span.filterOp').textContent}
    }
    get parameter() {
        return {value: this.base.querySelector('span.filterValue').textContent}
    }
    get properties() {
        return {
            field: this.field.value,
            operator: this.operator.value,
            parameter: this.parameter.value
        }
    }
}

let filterManager = {
    get filterElements() {
        return [...document.querySelectorAll('div.cfPanel_fieldFilterSection div.filterItemContainer')]
    },
    get filters() {
        let filterElements = [...document.querySelectorAll('div.cfPanel_fieldFilterSection div.filterItemContainer')]
        return filterElements.map(e => new Filter(e))
    },
    set filters(parameters) {
        this.updateFilters(parameters)
        console.log(`FILTERS UPDATED updated`)
    },
    get reportFilters() {
        let elements = [...document.querySelectorAll('div#contentWrapper form div.bGeneratedReport div.reportHeader div.filterHeader tr td')].filter(f => f.noWrap);
        return elements.map(e => new ReportFilter(e))
    },
    get allFilters() {
        let filters
        if (this.filters.length > 0)
            filters = this.filters
        else
            filters = this.reportFilters
        console.log(`filters=`,filters)
        return filters.map(f => f.properties)
    },
    get parameters() {
        let builderFilters = this.filters
        let reportFilters = this.reportFilters
        console.log(`have ${builderFilters.length} builder filters and ${reportFilters.length} report filters`)
        let filters = reportFilters.length > 0 ? reportFilters : builderFilters
        // set filters to builderFilters or reportFilters whichever isn't an empty array
        let min, max, equals, startsWith
        return {
            get customerNumber() {
                let filter = filters.find(f => f.field.value === 'Customer Number')
                if (filter)
                    return filter.parameter
            },
            get lastName() {
                let propFilters = filters.filter(f => f.field.value === 'Last Name')
                propFilters.forEach(f => {
                    let operator = f.operator.value
                    console.log(`operator=${operator} (${operator==='equals'})`)
                    let param = f.parameter.value
                    console.log(`param=${param}`)
                    if (operator === 'greater or equal') {
                        console.log('have "greater or equal"')
                        if (!min || param.charCodeAt(0) > parseInt(min)) {
                            min = param;
                        }
                    }
                    if (operator === 'less or equal') {
                        console.log('have "less or equal"')
                        if (!max || param.charCodeAt(0) < parseInt(max)) {
                            max = param;
                        }
                    }
                    if (operator === 'equals') {
                        console.log('have "equal"')
                        equals = param;
                    }
                    if (operator === 'starts with') {
                        console.log(`have "startsWith"`)
                        startsWith = param;
                    }
                })
                return {min: min, max: max, equals: equals, startsWith: startsWith}
            },
            set lastName(value) {

            },
            get zip() {
                // let min, max, equals
                let zipFilters = filters.filter(f => f.field.value === 'Zip/Postal Code')
                console.log(`have ${zipFilters.length} zip filters`)
                zipFilters.forEach(f => {
                    let operator = f.operator.value
                    console.log(`operator=${operator} (${operator==='equals'})`)
                    let param = f.parameter.value
                    console.log(`param=${param}`)
                    if (operator === 'greater or equal') {
                        console.log('have "greater or equal"')
                        if (!min || parseInt(param) > parseInt(min)) {
                            min = param;
                        }
                    }
                    if (operator === 'less or equal') {
                        console.log('have "less or equal"')
                        if (!max || parseInt(param) < parseInt(max)) {
                            max = param;
                        }
                    }
                    if (operator === 'equals') {
                        console.log(`have "equals"`)
                        equals = param
                    }
                });
                return {min: min, max: max, equals: equals}
            }
        }
    },
    get filtersEditable(){
        for (let i=0; i<this.filters.length; i++){
            let filter = this.filters[i]
            let mode = filter.mode === 'detail'
            let editButton = filter.buttons.edit
            let editButtonUsable = editButton.offsetParent
            if (!(filter && mode && editButton && editButtonUsable)){
                return false
            }
        }
        return true
    },
    async updateFilters(newProperties){
        let existingFilters = this.filters
        // remove matching filters and params
        console.log(`starting with ${newProperties.length} parameters and ${existingFilters.length} filters`)
        for (let i = newProperties.length - 1; i >= 0; i--) {
            let param = newProperties[i];

            for (let j = existingFilters.length - 1; j >= 0; j--) {
                let filter = existingFilters[j];

                if (filter.match(param)) {
                    newProperties.splice(i, 1);
                    existingFilters.splice(j, 1);
                    break;
                }
            }
        }
        console.log(`ending with ${newProperties.length} parameters and ${existingFilters.length} filters`)
        // remove excess filters
        if (existingFilters.length > newProperties.length) {
            let numFiltersToRemove = existingFilters.length - newProperties.length
            for (let i = 0; i < numFiltersToRemove; i++) {
                await existingFilters.pop().remove()
            }
        }
        // create missing filters
        if (existingFilters.length < newProperties.length) {
            let numFiltersToCreate = newProperties.length - existingFilters.length
            for (let i = 0; i < numFiltersToCreate; i++) {
                console.log(`creating filter ${i+1}`)
                let params = newProperties.pop()
                console.log(`params in filterManager=`,params)
                await Filter.create(params)
                console.log(`filter ${i+1} created`)
            }
        }
        // update existing filters
        console.log(`have ${newProperties.length} properties to update`)
        while (newProperties.length > 0){
            let params = newProperties.pop();
            let filter = existingFilters.pop()
            let updated = filter.update(params)
            console.log(`for ${newProperties.indexOf(params)}/${newProperties.length} updated`,updated)
            await updated
            console.log(`${newProperties.indexOf(params)}/${newProperties.length} updated`)
            console.log(`other filters editable=${this.filtersEditable}`)
        }
    },
    dates:  {
        get form() {
            let builderForm = document.querySelector('form.dateFiltersBody')
            let reportForm = document.querySelector('form#report')
            return reportForm || builderForm
        },
        get startElement() {
            let builderFilter = document.querySelector('form.x-panel-body.dateFiltersBody input[name="startDate"]')//
            console.log(`builderFilter=`,builderFilter)
            let reportFilter = document.querySelector('fieldSet input#sdate')//
            console.log(`reportFilter=`,reportFilter)
            return reportFilter || builderFilter
        },
        get start() {
            return this.startElement.value
        },
        set start(value) {
            this.startElement.value = value.replace(/"([^"]+(?="))"/g, '$1')
            this.startElement.dispatchEvent(helpers.pressEnter)
            if (document.querySelector('fieldSet input#sdate'))
                this.form.submit()
        },
        get endElement() {
            let builderFilter = document.querySelector('form.x-panel-body.dateFiltersBody input[name="endDate"]')
            let reportFilter = document.querySelector('fieldSet input#edate')
            return reportFilter || builderFilter
        },
        get end() {
            return this.endElement.value
        },
        set end(value) {
            this.endElement.value = value.replace(/"([^"]+(?="))"/g, '$1')
            this.endElement.dispatchEvent(helpers.pressEnter)
            if (document.querySelector('fieldSet input#edate'))
                this.form.submit()
        }
    }
}

let helpers = {
    pressEnter: new KeyboardEvent('keydown', {
        key: 'Enter',
        code: 'Enter',
        which: 13,
        keyCode: 13,
    })
}
