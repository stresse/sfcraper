#!/usr/bin/env osascript -l JavaScript

(() => {
    'use strict';
    let DEBUG = true
    function logDebug(string){
        let timeStamp = (new Date()).toLocaleTimeString()
        let caller = (new Error()).stack.split('\n')[1].replace('@', '')
        // pad string with spaces so that the length of string caller is 25
        if (DEBUG)
            console.log(`[${timeStamp}] ${helpers.padString(caller,13)} : ${string}`)
    }


    const files = {
        // a bunch of obj c functions for
        // interacting with files/folder
        appendToFile: function (strPath, strContents) {
        // Convert contents to NSString object
        let fileManager = $.NSFileManager.defaultManager
        // let fileHandler =$.NSFileHandle.fileHandleForUpdatingAtPath

        // const nsContent = $.NSString.alloc().initWithUTF8String_(strContents);
        const nsContent = $.NSString.stringWithString(strContents).dataUsingEncoding($.NSUTF8StringEncoding);
        // Normalize the file path
        const nsPath = $(strPath).stringByStandardizingPath;
        // Get the file manager
        // const fileManager = $.NSFileManager.defaultManager;

        // Check if the file exists
        const fileExists = fileManager.fileExistsAtPath(nsPath);

        if (!fileExists) {
            console.log(`${strPath} doesn't exist---creating it`)
            files.writeFile(strPath, strContents)
        }
        else {
            // Open the file for appending
            const fileHandler = $.NSFileHandle.fileHandleForUpdatingAtPath(nsPath);
            if (!fileHandler) {
                throw new Error(`Failed to open file for appending: ${strPath}`);
            }
            // Get to the end of the file
            fileHandler.seekToEndOfFile;
            // Convert new line to NSData
            const newLineData = ObjC.unwrap($.NSString.stringWithString('\n').dataUsingEncoding($.NSUTF8StringEncoding))
            fileHandler.writeData(newLineData);
            fileHandler.writeData(nsContent);
            // Close the file
            fileHandler.closeFile;
        }
    },
        readFile:function(strPath){
            const
                error = $(),
                str = ObjC.unwrap(
                    $.NSString.stringWithContentsOfFileEncodingError(
                        $(strPath)
                            .stringByStandardizingPath,
                        $.NSUTF8StringEncoding,
                        error
                    )
                );
            return Boolean(error.code) ? (
                ObjC.unwrap(error.localizedDescription)
            ) : str;
        },
        writeFile:function(strPath, strContents) {
            /* @File @Write @ObjC
            VER: 2.0  2017-03-18
            PARAMETERS:
              strPath    | string  | Path of file to write.  May use tilde (~)
              strContents  |  string  |  String to be output to file.
            */
            let nsContent       = $.NSString.alloc.initWithUTF8String(strContents)
            let nsPath      = $(strPath).stringByStandardizingPath
            //--- WRITE TO FILE ---
            //      Returns true IF successful, ELSE false
            let successBool  = nsContent.writeToFileAtomicallyEncodingError(nsPath, false, $.NSUTF8StringEncoding, null)
            if (!successBool)
                throw new Error("function writeFile ERROR:\nWrite to File FAILED for:\n" + strPath)
            return successBool
        },
        createFolder:function(strPath, createIntermediateDirectories) {
            let error = $()
            let fileManager = $.NSFileManager.defaultManager
            fileManager.createDirectoryAtPathWithIntermediateDirectoriesAttributesError(
                $(strPath).stringByStandardizingPath,
                createIntermediateDirectories,
                $(),
                error)
        },
        createFolderIfNotExists:function(strPath) {
            let folderExists = files.doesFolderExist(strPath)
            if (!folderExists) {
                files.createFolder(strPath)
            }
        },
        doesFileExist:function(strPath) {
            let error = $();
            return (
                $.NSFileManager.defaultManager
                    .attributesOfItemAtPathError(
                        $(strPath)
                            .stringByStandardizingPath,
                        error
                    ),
                error.code === undefined
            );
        },
        doesFolderExist:function(strPath) {
            let dm = $.NSFileManager.defaultManager
            let ref = Ref()
            $.NSFileManager.alloc.init.fileExistsAtPathIsDirectory(
                // standardize strPath (so it will recognize user paths and whatnot)
                $(strPath).stringByStandardizingPath,
                ref)
            return ref[0]
        },
    }
    const chrome = (function(){
        let app = Application('Google Chrome')
        app.includeStandardAdditions = true;
        function findTab({title=null,url=null}={}){
            return chrome.findTabs({title:title,url:url})[0]
        }
        function findTabs({title=null,url=null}={}){
            let tabs = []
            chrome.app.windows().forEach(
                win => {
                    if (title) {
                        win.tabs()
                            .filter(t => t.title() === title)
                            .forEach(t => tabs.push(t))
                    }
                    else if (url)
                        win.tabs()
                            .filter(t => t.url() === url)
                            .forEach(t=> tabs.push(t))
                })
            return tabs
        }
        return {app,findTabs,findTab}
    })()
    const helpers = {
        addToCSV(filePath, values) {
            files.appendToFile(filePath, values
                .map(r =>
                    // wrap the values in quotation marks
                    r.map(v=>`"${v}"`)
                        // join the values with commas
                        .join(','))
                // join the lines
                .join('\n'));
        },
        areArraysEqual(arr1, arr2) {
            if (!arr1 || !arr2)
                return false
            if (arr1.length !== arr2.length) {
                return false;
            }

            const sortedArr1 = arr1.slice().sort((a, b) => JSON.stringify(a) > JSON.stringify(b) ? 1 : -1);
            const sortedArr2 = arr2.slice().sort((a, b) => JSON.stringify(a) > JSON.stringify(b) ? 1 : -1);

            return JSON.stringify(sortedArr1) === JSON.stringify(sortedArr2);
        },
        getNextDate(dateString) {
            let filterDate = new Date(dateString)
            filterDate.setDate(filterDate.getDate() + 1)
            return filterDate.toLocaleDateString()
        },
        padString(string, length) {
            if (string.length >= length) {
                return string;
            }

            const spacesToAdd = length - string.length;
            const padding = ' '.repeat(spacesToAdd);

            return padding + string;
        },
        splitInterval(bottom, top, increments) {
            bottom = parseInt(bottom)
            top = parseInt(top)
            logDebug(`top=${top}\tbottom=${bottom}`)
            const intervalSize = Math.ceil((top - bottom + 1) / increments);
            console.log(intervalSize)
            const intervals = [];

            let start = bottom;
            let end = 0;

            for (let i = 1; i <= increments; i++) {
                end = start + intervalSize - 1;

                if (end > top) {
                    end = top;
                }
                logDebug(`start=${start},end=${end}`)
                intervals.push({
                    start: helpers.zipNumberToString(start),
                    end: helpers.zipNumberToString(end),
                });

                start = end + 1;
            }

            return intervals;
        },
        zipNumberToString(number) {
            return number.toString().padStart(5,'0')
        },
        zipStringToNumber(string){
            return parseInt(string)
        }
    }
    class Report {
        constructor() {
            this.tab = chrome.findTab({title: 'my leads ~ Salesforce - Unlimited Edition'})
            this.browserJSFile = `~/Coding/javascript/SFcraper/browser.js`
        }
        executeJS(script){
            // define functions using browserJSFile
            this.tab.execute({javascript : files.readFile(this.browserJSFile)});
            return this.tab.execute({javascript:script})
        }
        waitForPageToLoad() {
            delay(.2)
            while (this.tab.loading()){
                delay(1)
            }
        }
        waitForPropertyUpdate(value){
            let prop = (new Error()).stack.split('\n')[1].replace('@', '')
            let secondsToWait = 12
            // wait for tab to finish loading
            while (this.tab.loading()) {
                delay(.5)
            }
            let valueUpdated = () => {
                if (Array.isArray(value)) {
                    return helpers.areArraysEqual(this[prop],value)
                }
                else {
                    return this[prop] === value
                }
            }
            // get time we started waiting
            let startWaitTime = Date.now()
            let waitTime = () => (Date.now() - startWaitTime) / 1000
            // wait for property to update or time to run out
            while (!valueUpdated()){
                // wait for secondsToWait
                if (waitTime() < secondsToWait) {
                    // console.log(`[${prop}] current value=${JSON.stringify(this[prop])}\ttarget value=${JSON.stringify(value)}\tloading=${this.tab.loading()}`)
                    delay(1)
                }
                // give up and try setting property again
                else {
                    return this[prop] = value
                }
            }
        }

        /**
         * @description Figure out what the file name, path, etc. should be
         *              based on report attributes
         * note: this allows us to check if a report exists before/without
         *       generating a report AND, by using this function in the get
         *       methods for fileName and fileDirectory, we ensure consistent
         *       conventions
         * @param zip {Number} - zip code of the report
         * @param firstOfLastName {string} - first letter of last name
         * @returns {{  folder: string,
         *              filename: string,
         *              filepath: string,
         *              exists: boolean     }}
         */
        getFileInfo(options= {}){
            let folder, fileName, filePath
            if (this.state) {
                folder = `~/Documents/solar/customers`
                fileName = `${this.state} - all.csv`
            }
            filePath = `${folder}/${fileName}`
            return {
                folder: folder,
                fileName: fileName,
                filePath: filePath,
                exists: files.doesFileExist(filePath),
            }
        }
        popInZips(zipStart, zipEnd){
            let zipInfo = this.stateZips
            let zips = zipInfo.zips.map(z=>z.zip)
            let startIndex = zips.indexOf(zipStart)
            let endIndex = zips.indexOf(zipEnd)
            let population = 0
            for (let i = startIndex; i<=endIndex; i++){
                let zip = zipInfo.zips[i]
                let zipPopulation = zip.population
                population += zip.population
            }
            return population
        }
        getNextZip(zip) {
            let zips = this.stateZips.zips.map(z => z.zip)
            for (let i=0; i<zips.length; i++) {
                if (helpers.zipStringToNumber(zips[i]) > helpers.zipStringToNumber(zip)) {
                    return zips[i]
                }
            }
            return zips [zips.length - 1]
        }
        getEndZip({zipStart, populationTarget, maxZip=null}={}){
            let zipInfo = this.stateZips
            let zips = zipInfo.zips.map(z=>z.zip)
            let index = zips.indexOf(zipStart)
            let population = 0
            maxZip = maxZip || zipInfo.zips.slice(-1)[0].zip
            // start with ending as first element
            let zipEnd = zipInfo.zips[index]
            logDebug(`maxZip=${maxZip}`)
            // while population is below target and have remaining zips to add
            logDebug(`index=${index}\t#zips=${zipInfo.zips.length}\tpopulation=${population}\tpopulationTarget=${populationTarget}`)
            while ((population <= .95*populationTarget) && (zipInfo.zips[index].zip < maxZip)){
                zipEnd = zipInfo.zips[index]
                population += zipEnd.population
                logDebug(`index=${index},zipEnd=${zipEnd.zip},population=${population} (target=${.95*populationTarget})`)
                index ++
            }
            return zipEnd.zip
        }
        splitByZip(minZip, maxZip, desiredRecordCount){
            let intervalPopulation = this.popInZips(minZip,maxZip)
            this.createReport({minZip:minZip,maxZip:maxZip})
            let intervalRecordCount = this.recordCount
            let intervalRecordPercentage = intervalRecordCount / intervalPopulation
            let desiredPopulation = desiredRecordCount / intervalRecordPercentage
            let intervals = []
            let startZip = minZip
            let keepSplitting = true
            while (keepSplitting){
                let endZip = this.getEndZip({zipStart:startZip, populationTarget:desiredPopulation, maxZip:maxZip})
                keepSplitting = endZip < maxZip
                intervals.push({start:startZip,end:endZip})
                if (keepSplitting)
                    startZip = this.getNextZip(endZip)
            }
            logDebug(`have ${intervals.length} intervals`)
            return intervals
        }
        createReport(options){
            logDebug(JSON.stringify(options))
            this.dateStart = options.dateStart || ''
            this.dateEnd = options.dateEnd || ''
            if (options.state)
                this.state = options.state
            let filters = []
            if (options.minZip){
                filters.push({
                    field: 'Zip/Postal Code',
                    operator: 'greater or equal',
                    parameter: options.minZip
                })
            }
            if (options.maxZip){
                filters.push({
                    field: 'Zip/Postal Code',
                    operator: 'less or equal',
                    parameter: options.maxZip
                })
            }
            if (options.zip){
                filters.push({
                    field: 'Zip/Postal Code',
                    operator: 'equals',
                    parameter: options.zip
                })
            }
            if (options.firstOfLastName){
                filters.push({
                    field: 'Last Name',
                    operator: 'starts with',
                    parameter: options.firstOfLastName
                })
            }
            this.filters = filters
            return true
        }
        runReport() {
            switch (this.page){
                case 'report':
                    this.tab.execute({javascript:`document.querySelector('div.reportActions input[title="Run Report"]').click()`})
                    break
                case 'edit':
                    this.tab.execute({javascript:`document.querySelector('div#mainToolbar table button.x-btn-text.run-report-btn-icon').click()`})
                    break
            }
            this.waitForPageToLoad()
        }
        addRowsToCSV(filePath,rowValues) {
            files.appendToFile(filePath, rowValues
                .map(r =>
                    // wrap the values in quotation marks
                    r.map(v=>`"${v}"`)
                        // join the values with commas
                        .join(','))
                // join the lines
                .join('\n'));
        }
        scrapeReport() {
            let initialDateStart = this.dateStart
            let initialDateEnd = this.dateEnd
            // get index of "Create Date"
            let createdIndex = this.columns.indexOf(this.sortedBy.value)
            // initialize some variables
            let keepScraping = true
            let rowsToCheck = []
            let rowsToCheckNext = []
            // start scraping loop
            logDebug(`this.dateStart=${this.dateStart}\tthis.maxDateCreated=${this.maxDateCreated}\tthis.recordCount=${this.recordCount}`)
            while (this.recordCount > 2000) {
                if (this.dateStart === this.maxDateCreated) {
                    let scrapedInTwo = this.splitInTwo()
                    if (!scrapedInTwo) {
                        let minZip = this.minZip
                        let maxZip = this.maxZip
                        let desiredRecordCount = this.recordCount / 2
                        let intervals = this.splitByZip(minZip,maxZip,desiredRecordCount)
                        intervals.forEach(interval => {
                            this.createReport({minZip:interval.start,maxZip:interval.end})
                            this.scrapeReport()
                        })
                    }
                }
                else {
                    logDebug(`this.dateStart=${this.dateStart}\tthis.maxDateCreated=${this.maxDateCreated}\tthis.recordCount=${this.recordCount}`)
                    rowsToCheck = rowsToCheckNext
                    rowsToCheckNext = []
                    let newValues = []
                    let dateStart = this.dateStart
                    let dateMax = this.maxDateCreated
                    this.rowValues.forEach(row => {
                        if (dateStart && (row[createdIndex] === dateStart)) {
                            if (!rowsToCheck.some(r => JSON.stringify(r) === JSON.stringify(row))) {
                                newValues.push(row)
                            }
                        }
                        else {
                            newValues.push(row)
                        }
                        // add all rows with max dateStart value to be checked for duplicates
                        // next loop
                        if (row[createdIndex] === dateMax) {
                            rowsToCheckNext.push(row)
                        }
                    })
                    logDebug(`adding ${newValues.length} records to ${this.filePath}`)
                    this.addRowsToCSV(this.filePath, newValues)
                    this.dateStart = dateMax
                }
            }
        }
        scrapeState() {
            let zipInfo = this.stateZips
            let minZip = zipInfo.minZip
            let maxZip = zipInfo.maxZip
            // generate report for entire state
            let generated = this.createReport({minZip:minZip, maxZip: maxZip})
            if (generated) {
                let intervals = this.splitByZip(minZip,maxZip,40000)
                intervals.forEach(interval => {
                    this.createReport({minZip:interval.start,maxZip:interval.end})
                    this.scrapeReport()
                })
            }
            logDebug(`DONE SCRAPING`)
        }
        splitInTwo(){
            this.dateEnd = this.dateStart
            if (this.recordCount <= 4000) {
                // add a filter to report.filters (for tracking purposes)
                let newValues = []
                this.sortedBy = { value: 'Last Name', operation: 'ascending' }
                newValues = this.rowValues
                this.sortedBy = { value: 'Last Name', operation: 'descending' }
                // add all new rowValues to allValues
                this.rowValues.forEach(row => {
                    if (!newValues.some(r => JSON.stringify(r) === JSON.stringify(row))) {
                        newValues.push(row)
                    }
                })
                // save values to filePath
                helpers.addToCSV(this.filePath, newValues)
                this.sortedBy = {value: 'Create Date', operation: 'ascending'}
                this.dateStart = helpers.getNextDate(this.dateStart)
                this.dateEnd = ''
                return true
            }
            else {
                this.dateEnd = ''
                return false
            }
        }
        get columns() {
            return this.tab.execute({javascript: `[...document.querySelectorAll('div.reportOutput div#fchArea table tbody tr.headerRow th')].map(e=>e.textContent)`})
        }
        get dateEnd() {
            return this.executeJS(`filterManager.dates.end`)
        }
        set dateEnd(date) {
            if (this.dateEnd !== date){
                this.executeJS(`filterManager.dates.end='${date}'`)
                this.waitForPropertyUpdate(date)
            }
        }
        get dateStart() {
            return this.executeJS(`filterManager.dates.start`)
        }
        set dateStart(date) {
            // check if a change is needed
            if (this.dateStart!==date){
                this.executeJS(`filterManager.dates.start = '${date}'`)
                delay(1)
                this.waitForPropertyUpdate(date)
            }
        }
        get directory(){
            return this.getFileInfo({
                zip:this.zip,
                firstOfLastName:this.firstOfLastName,
                state: this.state
            }).folder
        }
        get fileName(){
            return this.getFileInfo({
                zip: this.zip,
                firstOfLastName: this.firstOfLastName,
                state: this.state
            }).fileName
        }
        get filePath() {
            return `${this.directory}/${this.fileName}`
        }
        get filters() {
            return this.executeJS(`filterManager.allFilters`)
        }
        set filters(newFilters) {
            if (!helpers.areArraysEqual(this.filters,newFilters)) {
                this.page = 'edit'
                this.executeJS(`filterManager.updateFilters(${JSON.stringify(newFilters)})`)
                this.waitForPropertyUpdate(newFilters)
                this.page = 'report'
            }
            else {
                console.log(`filters are already what we want!`)
            }
        }
        get firstOfLastName(){
            return this.executeJS({javascript: "filterManager.parameters.lastName.startsWith"})
        }
        get maxDateCreated() {
            this.sortedBy = {value:'Create Date',operation:'ascending'}
            let colIndex = this.columns.indexOf('Create Date')
            let rowValues = this.rowValues
            if (rowValues && rowValues[rowValues.length - 1])
                return rowValues[rowValues.length - 1][colIndex]
            else
                return null
        }
        get maxZip(){
            return this.executeJS(`filterManager.parameters.zip.max`)
        }
        get minZip(){
            return this.executeJS(`filterManager.parameters.zip.min`)
        }
        get page() {
            // check if there is a "run report" button
            let runReportButton = this.executeJS("document.querySelector('div#mainToolbar table button.x-btn-text.run-report-btn-icon')")
            // check if there is an "edit" button
            let editButton = this.executeJS("document.querySelector(\'div#contentWrapper div.filterHeader span.filteredBy\').parentElement.querySelector(\'a\');")
            if (runReportButton)
                return 'edit'
            else if (editButton)
                return 'report'
            else
                return null
        }
        set page(pageName) {
            // if we are not already on the right page
            if (this.page !== pageName){
                switch (pageName) {
                    case 'edit':
                        if (this.page !== 'edit')
                            this.executeJS("document.querySelector('div#contentWrapper div.filterHeader span.filteredBy').parentElement.querySelector('a').click()")
                        break
                    case 'report':
                        if (this.page !== 'report')
                            this.executeJS("document.querySelector('div#mainToolbar table button.x-btn-text.run-report-btn-icon').click()")
                        break
                }
                this.waitForPageToLoad()
                this.waitForPropertyUpdate(pageName)
            }
        }
        get recordCount() {
            let recordText, recordCount
            this.waitForPageToLoad()
            if (this.page === 'report'){
                recordText = this.tab.execute({javascript: `document.querySelector("#fchArea > table > tbody > tr.grandTotal.grandTotalTop > td").textContent`})
            }
            if (this.page === 'edit'){
                recordText = this.tab.execute({javascript:`document.querySelector('div#dataPreviewPanel div#gridViewpreviewPanelGrid div.rb-count-row').textContent`})
            }
            recordCount = Number(recordText.replace(/,/g, '').match(/[\d,]+/))
            return recordCount
        }
        get reportRecords() {
            /** @todo
             * implement a function where if we get stuck on a Created By date
             * we set end date to same date and see if there are <4k records
             * if there are, use last name sort to scrape first two thousand
             * then second two thousand
             */
            // sort by create date (in case we need to pull >2k records
            this.sortedBy = {
                value:'Create Date',
                operation:'ascending'
            }
            console.log('sort value is correct')
            let sortColumnIndex = this.columns.indexOf(this.sortedBy)
            let allValues = [this.columns]
            let keepScraping = true
            let previousStartDateRecords = []
            let rowsSameCreateDate = []
            while (keepScraping){
                let newValues = []
                // check start date (to avoid duplicates later)
                let startDate = this.dateStart
                // get index of "Create Date"
                let createdIndex = this.columns.length + sortColumnIndex
                // add all values not already included
                this.rowValues.forEach(row => {
                    allValues.push(row)
                    // if start date
                    if (startDate){
                        // check if row is a duplicate
                        if (row[createdIndex]===startDate) {
                            // if not a duplicate
                            if (!rowsSameCreateDate.some(r => JSON.stringify(r) === JSON.stringify(row)))
                                // add value
                                newValues.push(row)
                        }
                        // otherwise, just add the fucking value
                        else {
                            newValues.push(row)
                        }
                    }
                    // if there isn't a start date, just add the row
                    else {
                        newValues.push(row)
                    }
                })
                console.log(`adding ${newValues.length} new records ${this.recordCount} remaining`)
                // save all new records in allValues to a JSON file
                if (newValues.length === 0){
                    // handle exceptions when we have WAY too many records with the same created by date
                    this.splitByLetter(this.filePath)
                }
                // keep scraping if the record count exceeds the 2000 limit
                files.appendToFile(this.filePath, newValues
                    .map(r =>
                        // wrap the values in quotation marks
                        r.map(v=>`"${v}"`)
                            // join the values with commas
                            .join(','))
                    // join the lines
                    .join('\n'));
                // add values from our max created by date to list to avoid duplicates on next scrape
                let dateCreatedMax = this.maxDateCreated
                rowsSameCreateDate = allValues.filter(r=> r[createdIndex]===dateCreatedMax)
                keepScraping = this.recordCount >= 2000
                if (keepScraping){
                    allValues = []
                    this.dateStart = dateCreatedMax
                }
            }
            console.log(`done scraping values`)
        }
        get rowValues() {
            return this.tab.execute({javascript: '[...document.querySelectorAll(\'div.reportOutput div#fchArea table tbody tr.odd, tr.even\')].map(row => [...row.querySelectorAll(\'td\')].map(e => e.textContent))'})
        }
        get sortedBy() {
            let sortText, sortValue, sortOperation
            switch (this.page){
                case 'edit':
                    sortValue = this.tab.execute({
                        javascript: `document.querySelector('div#gridViewpreviewPanelGrid tr.x-grid3-hd-row td[class*="sort"]').textContent`
                    })
                    let operationText = report.tab.execute({javascript:`document.querySelector('div#gridViewpreviewPanelGrid tr.x-grid3-hd-row td[class*="sort"]').className`})
                    if (operationText.includes('sort-desc')) {
                        sortOperation = 'descending'
                    }
                    else {
                        sortOperation = 'ascending'
                    }
                    break
                case 'report':
                    sortText = this.tab.execute({javascript: `document.querySelector('th[scope=col] a[title*="Sorted"]').getAttribute('title')`})
                    if (sortText){
                        let sortValueSearch = sortText.match(/^.*?(?=\s-\sSorted)/)
                        sortValue = sortValueSearch ? sortValueSearch[0] : null
                        let sortOperationSearch = sortText.match(/Sorted\s(ascending|descending)/)
                        sortOperation = sortOperationSearch ? sortOperationSearch[1] : null
                    }
                    break
                default:
                    console.log(`no sort value to grab`)
            }
            return {
                value: sortValue,
                operation: sortOperation
            }
        }

        set sortedBy({value=null,operation='ascending'}){
            // if either the sort value or operation aren't right,
            // click the appropriate column
            if (!((this.sortedBy.value === value) && (this.sortedBy.operation === operation))){
                switch (this.page){
                    case 'edit':
                        this.tab.execute({
                            javascript:`[...document.querySelectorAll('div#gridViewpreviewPanelGrid tr.x-grid3-hd-row td div')]
                            .find(e => e.textContent.includes('${value}'))
                                .click()`
                        })
                        break
                    case 'report':
                        this.tab.execute({
                            javascript: `[...document.querySelectorAll('th[scope=col] a')]
                            .find(e => e.textContent.includes('${value}'))
                                .click()`})
                        break
                    default:
                        console.log(`no sort value to grab`)
                }
                this.waitForPageToLoad()
                // just call again until desired result is reached
                this.sortedBy = {value:value,operation:operation}
            }
        }
        get state() {
            return this._currentState
        }
        set state(state) {
            this._currentState = state
        }
        get stateZips(){
            let zipInfo
            let zipFile = `~/Coding/javascript/SFcraper/info/zips/${this.state}.json`
            // gonna check if the info has already been fetched
            // to avoid unneccessary opening/closing/querying
            if (this._zipInfo && (this._zipInfo.state ===this.state)){
                console.log(`stateZips: using cache`)
                zipInfo = this._zipInfo
            }
            // if a file exists with zip info, get it from there
            else if (files.doesFileExist(zipFile)){
                zipInfo = JSON.parse(files.readFile(zipFile))
            }
            // otherwise create a tab to scrape it
            else {
                let zipTab
                let zipURL = `https://worldpopulationreview.com/zips/${this.state.toLowerCase().replace(' ','-')}`
                // search to see if zipTab exists
                zipTab = chrome.findTab({url:zipURL})
                // if it doesn't
                if (!zipTab){
                    // create it
                    zipTab = chrome.app.Tab({url:zipURL})
                    // activate it
                    chrome.app.windows[0].tabs.push(zipTab);
                    // wait for it to load
                    while (zipTab.loading())
                        delay(1)
                }
                function getZipData(){
                    return zipTab.execute({javascript:
                            `[...document.querySelectorAll('body div table tbody tr')]
                                .map(r=> 
                                    [...r.querySelectorAll('th, td')]
                                        .map(v=> v.textContent))
                                .map((rowValue) => ({
                                    zip: rowValue[0],
                                    city: rowValue[1],
                                    county:rowValue[2],
                                    population:Number(rowValue[3].replace(/,/g, ''))
                            }))`})
                }
                while (getZipData().length === 0){
                    delay(1)
                }
                let zipData = getZipData().sort((a,b) => parseInt(a.zip) - parseInt(b.zip))
                zipTab.close()
                let zipNumbers = zipData.map(zip => parseInt(zip.zip))
                let minZipNumber = Math.min(...zipNumbers)
                let maxZipNumber = Math.max(...zipNumbers)
                let minZip = minZipNumber.toString().padStart(5,'0')
                let maxZip = maxZipNumber.toString().padStart(5,'0')
                zipInfo = {
                    state:this.state,
                    zips:zipData,
                    minZip: minZip,
                    maxZip: maxZip
                }
                let zipInfoJSON = JSON.stringify(zipInfo)
                files.writeFile(zipFile,zipInfoJSON)
                this._zipInfo = zipInfo
            }
            return zipInfo
        }
        get zip() {
            return this.executeJS(`filterManager.parameters.zip`)
        }
    }

    let report = new Report()

    let states = ['Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

    states.forEach(state => {
        report.state = state
        report.scrapeState()
    })

    report.executeJS(`console.log('done')`)
    console.log('done')
})();